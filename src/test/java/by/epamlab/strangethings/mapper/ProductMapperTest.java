package by.epamlab.strangethings.mapper;

import by.epamlab.strangethings.dto.ProductDTO;
import by.epamlab.strangethings.model.Category;
import by.epamlab.strangethings.model.Price;
import by.epamlab.strangethings.model.Product;
import by.epamlab.strangethings.repository.CategoryDAO;
import by.epamlab.strangethings.repository.PriceDAO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.stream.Collectors;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProductMapperTest {
    @Autowired
    private CategoryDAO categoryDAO;

    @Autowired
    private PriceDAO priceDAO;

    @Autowired
    private TestEntityManager entityManager;

    private ProductMapper productMapper;
    private Product product;
    private ProductDTO productDTO;

    @Before
    public void setUp() {
        productMapper = new ProductMapper(priceDAO, categoryDAO);

        Price price = entityManager.persist(new Price("BYN", 410));
        Category category = entityManager.persist(new Category( "Category", null));
        product = entityManager.persist(new Product("Product", price, category));

        productDTO = new ProductDTO();
        productDTO.setId(product.getId());
        productDTO.setName(product.getName());
        productDTO.setPrices(product.getPrices()
                .stream()
                .map(Price::getId)
                .collect(Collectors.toList()));
        productDTO.setCategories(product.getCategories()
                .stream()
                .map(Category::getId)
                .collect(Collectors.toList()));
    }

    @After
    public void tearDown() {
        entityManager.clear();
    }

    @Test
    public void mapToDTOEquals() {
        assertEquals(productDTO, productMapper.mapToDTO(product));
    }

    @Test
    public void mapToModelEquals() {
        Product mappedProduct = productMapper.mapToModel(productDTO);

        assertEquals(productDTO.getId(), mappedProduct.getId());
        assertEquals(productDTO.getName(), mappedProduct.getName());
        assertArrayEquals(
                productDTO.getPrices().toArray(),
                mappedProduct.getPrices()
                        .stream()
                        .map(Price::getId)
                        .toArray()
        );
        assertArrayEquals(
                productDTO.getCategories().toArray(),
                mappedProduct.getCategories()
                        .stream()
                        .map(Category::getId)
                        .toArray()
        );
    }
}
