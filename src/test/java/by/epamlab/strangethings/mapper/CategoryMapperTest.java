package by.epamlab.strangethings.mapper;

import by.epamlab.strangethings.dto.CategoryDTO;
import by.epamlab.strangethings.model.Category;
import by.epamlab.strangethings.repository.CategoryDAO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CategoryMapperTest {
    @Autowired
    private CategoryDAO categoryDAO;

    @Autowired
    private TestEntityManager entityManager;


    private CategoryMapper categoryMapper;
    private Category superCategory;
    private Category subCategory;
    private CategoryDTO subCategoryDTO;

    @Before
    public void setUp() {
        categoryMapper = new CategoryMapper(categoryDAO);
        superCategory = entityManager.persist(new Category("Supercategory", null));
        subCategory = entityManager.persist(new Category("Subcategory", superCategory));

        subCategoryDTO = new CategoryDTO();
        subCategoryDTO.setId(subCategory.getId());
        subCategoryDTO.setName(subCategory.getName());
        subCategoryDTO.setSuperCategory(subCategory.getSuperCategory().getId());
    }

    @After
    public void tearDown() {
        entityManager.clear();
    }

    @Test
    public void mapToDTOEquals() {
        assertEquals(subCategoryDTO, categoryMapper.mapToDTO(subCategory));
    }

    @Test
    public void mapToDTOSupercategoryEquals() {
        Long superCategoryId = categoryMapper.mapToDTO(subCategory).getSuperCategory();
        assertEquals(superCategory, categoryDAO.findById(superCategoryId).orElse(null));
    }

    @Test
    public void mapToModelEquals() {
        assertEquals(subCategory, categoryMapper.mapToModel(subCategoryDTO));
    }

    @Test
    public void mapToModelSupercategoryEquals() {
        assertEquals(superCategory, categoryMapper.mapToModel(subCategoryDTO).getSuperCategory());
    }
}