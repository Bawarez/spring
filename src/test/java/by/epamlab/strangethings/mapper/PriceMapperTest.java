package by.epamlab.strangethings.mapper;

import by.epamlab.strangethings.dto.PriceDTO;
import by.epamlab.strangethings.model.Price;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class PriceMapperTest {
    private PriceMapper priceMapper = new PriceMapper();
    private Price price = new Price(1L, "BYN", 410);
    private PriceDTO priceDTO = new PriceDTO();

    {
        priceDTO.setId(price.getId());
        priceDTO.setCurrency(price.getCurrency());
        priceDTO.setValue(price.getValue());
    }

    @Test
    public void mapToDTOEquals() {
        assertEquals(priceDTO, priceMapper.mapToDTO(price));
    }

    @Test
    public void mapToModelEquals() {
        assertEquals(price, priceMapper.mapToModel(priceDTO));
    }
}