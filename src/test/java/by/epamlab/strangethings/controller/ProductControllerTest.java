package by.epamlab.strangethings.controller;

import by.epamlab.strangethings.dto.ProductDTO;
import by.epamlab.strangethings.facade.Facade;
import by.epamlab.strangethings.facade.ProductFacade;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


import javax.transaction.Transactional;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@Transactional
@SpringBootTest
@RunWith(SpringRunner.class)
public class ProductControllerTest extends ControllerTest {
    @Autowired
    private WebApplicationContext context;

    @Autowired
    private ProductFacade productFacade;

    private MockMvc mvc;
    private ProductDTO product;

    @Before
    public void setUp() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
        product = new ProductDTO();
        product.setId(1L);
        product.setName("Test");
        product.getPrices().add(1L);
        product.getCategories().add(1L);
    }

    @After
    public void tearDown() {
        product = null;
    }

    @Test
    @WithMockUser(username="zakatniy", roles="USER")
    public void getExist() throws Exception {
        getExist("/products/", product);
    }

    @Test
    @WithMockUser(username="admin", roles="ADMIN")
    public void deleteOK() throws Exception {
        mockMvc().perform(post("/products/delete")
                .param("id", "10")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username="zakatnyj", roles="USER")
    public void addForbidden() throws Exception {
        forbidden("/products/add");
    }

    @Test
    @WithMockUser(username="zakatnyj", roles="USER")
    public void updateForbidden() throws Exception {
        forbidden("/products/update");
    }

    @Test
    @WithMockUser(username="zakatnyj", roles="USER")
    public void deleteForbidden() throws Exception {
        forbidden("/products/update");
    }

    @Test
    public void getAllUnauthorized() throws Exception {
        mockMvc().perform(post("/products/all")
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username="zakatnyj", roles="USER")
    public void findByNameFound() throws Exception {
        String expected = mapper.writeValueAsString(new ProductDTO[]{product});
        mvc.perform(get("/products/search/name?name=" + product.getName())
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json(expected));
    }

    @Test
    @WithMockUser(username="zakatnyj", roles="USER")
    public void findByCategoryFound() throws Exception {
        String expected = mapper.writeValueAsString(new ProductDTO[]{product});
        mvc.perform(get("/products/search/category?id=" + product.getCategories().get(0))
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json(expected));
    }

    @Test
    @WithMockUser(username="zakatnyj", roles="USER")
    public void findByPriceFound() throws Exception {
        String expected = mapper.writeValueAsString(new ProductDTO[]{product});
        mvc.perform(get("/products/search/price?id=" + product.getPrices().get(0))
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json(expected));
    }


    @Test
    @WithMockUser(username="admin", roles="ADMIN")
    public void updateOK() throws Exception {
        ProductDTO oldState = new ProductDTO();
        oldState.setName("old");
        oldState.getPrices().add(5L);
        oldState.getCategories().add(4L);
        oldState = productFacade.create(oldState);
        ProductDTO updated = new ProductDTO();
        updated.setId(oldState.getId());
        updated.setName(oldState.getName());
        updated.getPrices().add(9L);
        updated.getCategories().add(20L);

        String expected = mapper.writeValueAsString(productFacade.update(updated));
        mvc.perform(post("/products/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content(expected))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is (oldState.getId().intValue())))
                .andExpect(jsonPath("$.name", is (oldState.getName())))
                .andExpect(jsonPath("$.prices", not(oldState.getPrices()
                        .stream()
                        .map(Long::intValue)
                        .collect(Collectors.toList()))))
                .andExpect(jsonPath("$.categories", not(oldState.getCategories()
                        .stream()
                        .map(Long::intValue)
                        .collect(Collectors.toList()))));
    }

    @Test
    @WithMockUser(username="admin", roles="ADMIN")
    public void addOK() throws Exception {
        ProductDTO newProduct = new ProductDTO();
        newProduct.setName("new");
        newProduct.getPrices().add(5L);
        newProduct.getCategories().add(4L);
        newProduct = productFacade.create(newProduct);

        String expected = mapper.writeValueAsString(newProduct);
        mvc.perform(post("/products/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(expected))
                .andExpect(status().isCreated())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is (newProduct.getId().intValue())))
                .andExpect(jsonPath("$.name", is (newProduct.getName())))
                .andExpect(jsonPath("$.prices", is(newProduct.getPrices()
                        .stream()
                        .map(Long::intValue)
                        .collect(Collectors.toList()))))
                .andExpect(jsonPath("$.categories", is(newProduct.getCategories()
                        .stream()
                        .map(Long::intValue)
                        .collect(Collectors.toList()))));
    }

    @Override
    protected MockMvc mockMvc() {
        return mvc;
    }

    @Override
    protected Facade facade() {
        return productFacade;
    }
}