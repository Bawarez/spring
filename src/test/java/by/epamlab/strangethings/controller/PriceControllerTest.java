package by.epamlab.strangethings.controller;

import by.epamlab.strangethings.dto.PriceDTO;
import by.epamlab.strangethings.facade.Facade;
import by.epamlab.strangethings.facade.PriceFacade;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@Transactional
@SpringBootTest
@RunWith(SpringRunner.class)
public class PriceControllerTest extends ControllerTest {
    @Autowired
    private WebApplicationContext context;

    @Autowired
    private PriceFacade priceFacade;

    private MockMvc mvc;

    @Before
    public void setUp() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    @WithMockUser(username="zakatnyj", roles="USER")
    public void getAllPricesReceived() throws Exception {
        getAllEntitiesReceived("/prices/all");
    }

    @Test
    @WithMockUser(username="zakatniy", roles="USER")
    public void getExist() throws Exception {
        PriceDTO price = new PriceDTO();
        price.setValue(410);
        price.setCurrency("BYN");

        getExist("/prices/", price);
    }

    @Test
    @WithMockUser(username="admin", roles="ADMIN")
    public void addOK() throws Exception {
        PriceDTO price = new PriceDTO();
        price.setCurrency("BYN");
        price.setValue(410);
        price = priceFacade.create(price);

        String expected = mapper.writeValueAsString(price);
        mvc.perform(post("/prices/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(expected))
                .andExpect(status().isCreated())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is (price.getId().intValue())))
                .andExpect(jsonPath("$.currency", is (price.getCurrency())))
                .andExpect(jsonPath("$.value", is(price.getValue())));
    }

    @Test
    @WithMockUser(username="admin", roles="ADMIN")
    public void updateOK() throws Exception {
        PriceDTO price = priceFacade.get(2L);
        price.setValue(404);


        String expected = mapper.writeValueAsString(priceFacade.create(price));
        mvc.perform(post("/prices/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content(expected))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is (price.getId().intValue())))
                .andExpect(jsonPath("$.currency", is (price.getCurrency())))
                .andExpect(jsonPath("$.value", is(price.getValue())));
    }

    @Test
    @WithMockUser(username="admin", roles="ADMIN")
    public void deleteOK() throws Exception {
        PriceDTO price = new PriceDTO();
        price.setCurrency("RUB");
        price.setValue(410);
        deleteOK("/prices/delete", price);
    }


    @Test
    @WithMockUser(username="zakatnyj", roles="USER")
    public void addForbidden() throws Exception {
        forbidden("/prices/add");
    }

    @Test
    @WithMockUser(username="zakatnyj", roles="USER")
    public void updateForbidden() throws Exception {
        forbidden("/prices/update");
    }

    @Test
    @WithMockUser(username="zakatnyj", roles="USER")
    public void deleteForbidden() throws Exception {
        forbidden("/prices/update");
    }


    @Test
    @WithMockUser(username="zakatnyj", roles="USER")
    public void findByProduct() throws Exception {
        PriceDTO price = new PriceDTO();
        price.setId(1L);
        price.setCurrency("BYN");
        price.setValue(410);

        String expected = mapper.writeValueAsString(new PriceDTO[]{price});
        mvc.perform(get("/prices/search?product=" + 1)
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json(expected));
    }

    @Test
    @WithMockUser(username="zakatnyj", roles="USER")
    public void findPricesByRange() throws Exception {
        int min = 30;
        int max = 70;
        String currency = "BYN";
        List<PriceDTO> prices = priceFacade.findByRange(min, max, currency, pageable);
        String expected = mapper.writeValueAsString(prices);

        mvc.perform(get("/prices/search/" + currency + "?min=" + min + "&max=" + max)
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json(expected));
    }

    @Test
    @WithMockUser(username="zakatnyj", roles="USER")
    public void findPricesByCurrency() throws Exception {
        String currency = "BYN";
        List<PriceDTO> prices = priceFacade.findByCurrency(currency, pageable);
        String expected = mapper.writeValueAsString(prices);

        mvc.perform(get("/prices/search/" + currency)
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json(expected));
    }

    @Override
    protected MockMvc mockMvc() {
        return mvc;
    }

    @Override
    protected Facade facade() {
        return priceFacade;
    }
}