package by.epamlab.strangethings.controller;

import by.epamlab.strangethings.dto.DTO;
import by.epamlab.strangethings.facade.Facade;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.http.codec.json.Jackson2JsonEncoder;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public abstract class ControllerTest {
    protected Pageable pageable = PageRequest.of(0, 20, Sort.Direction.ASC, "id");
    protected ObjectMapper mapper = new Jackson2JsonEncoder().getObjectMapper();

    protected void getAllEntitiesReceived(String url) throws Exception {
        String expected = mapper.writeValueAsString(facade().getAll(pageable));
        mockMvc().perform(get(url)
                .accept (MediaType.APPLICATION_JSON))
            .andExpect(status().isOk()).andExpect(content().json(expected));
    }

    protected void getExist(String url, DTO dto) throws Exception {
        DTO savedDTO = facade().create(dto);
        String expected = mapper.writeValueAsString(savedDTO);

        mockMvc().perform(get(url + savedDTO.getId())
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json(expected));
    }

    protected void deleteOK(String url, DTO dto) throws Exception {
        DTO savedDTO = facade().create(dto);
        mockMvc().perform(post(url)
                .param("id", savedDTO.getId().toString())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    protected void forbidden(String url) throws Exception {
        mockMvc().perform(post(url)
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }


    protected abstract MockMvc mockMvc();
    protected abstract Facade facade();
}
