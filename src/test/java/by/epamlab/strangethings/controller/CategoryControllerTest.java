package by.epamlab.strangethings.controller;

import by.epamlab.strangethings.dto.CategoryDTO;
import by.epamlab.strangethings.facade.CategoryFacade;
import by.epamlab.strangethings.facade.Facade;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


import javax.transaction.Transactional;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.*;

@Transactional
@SpringBootTest
@RunWith(SpringRunner.class)
public class CategoryControllerTest extends ControllerTest {
    @Autowired
    private WebApplicationContext context;

    @Autowired
    private CategoryFacade categoryFacade;

    private MockMvc mvc;

    @Before
    public void setUp() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    @WithMockUser(username="zakatnyj", roles="USER")
    public void getAllCategoriesReceived() throws Exception {
        getAllEntitiesReceived("/categories/all");
    }

    @Test
    @WithMockUser(username="zakatniy", roles="USER")
    public void getExist() throws Exception {
        CategoryDTO category = new CategoryDTO();
        category.setName("Category");

        getExist("/categories/", category);
    }

    @Test
    @WithMockUser(username="admin", roles="ADMIN")
    public void addOK() throws Exception {
        CategoryDTO category = new CategoryDTO();
        category.setId(1L);
        category.setName("New category");

        String expected = mapper.writeValueAsString(category);
        mvc.perform(post("/categories/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(expected))
                .andExpect(status().isCreated())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is (category.getId().intValue())))
                .andExpect(jsonPath("$.name", is (category.getName())))
                .andExpect(jsonPath("$.superCategory", is(category.getSuperCategory())));
    }

    @Test
    @WithMockUser(username="admin", roles="ADMIN")
    public void updateOK() throws Exception {
        CategoryDTO category = categoryFacade.get(1L);
        category.setName("New name");

        String expected = mapper.writeValueAsString(categoryFacade.create(category));
        mvc.perform(post("/categories/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content(expected))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is (category.getId().intValue())))
                .andExpect(jsonPath("$.name", is (category.getName())))
                .andExpect(jsonPath("$.superCategory", is(category.getSuperCategory())));
    }

    @Test
    @WithMockUser(username="admin", roles="ADMIN")
    public void deleteOK() throws Exception {
        CategoryDTO category = new CategoryDTO();
        category.setName("name");
        deleteOK("/categories/delete", category);
    }

    @Test
    @WithMockUser(username="zakatnyj", roles="USER")
    public void addForbidden() throws Exception {
        forbidden("/categories/add");
    }

    @Test
    @WithMockUser(username="zakatnyj", roles="USER")
    public void updateForbidden() throws Exception {
        forbidden("/categories/update");
    }

    @Test
    @WithMockUser(username="zakatnyj", roles="USER")
    public void deleteForbidden() throws Exception {
        forbidden("/categories/update");
    }

    @Test
    @WithMockUser(username="zakatnyj", roles="USER")
    public void findCategories() throws Exception {
        String name = categoryFacade.getAll(pageable).get(0).getName();

        String expected = mapper.writeValueAsString(categoryFacade.findByName(name, pageable));
        mvc.perform(get("/categories/search?name=" + name)
                .accept (MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json(expected));
    }

    @Override
    protected MockMvc mockMvc() {
        return mvc;
    }

    @Override
    protected Facade facade() {
        return categoryFacade;
    }
}