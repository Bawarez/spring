package by.epamlab.strangethings.repository;

import by.epamlab.strangethings.model.Category;
import by.epamlab.strangethings.model.Price;
import by.epamlab.strangethings.model.Product;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;


import java.util.Arrays;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProductDAOTest {
    private Pageable pageable;

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private TestEntityManager entityManager;

    private Product product;

    @Before
    public void setUp() {
        pageable = PageRequest.of(0, 5, Sort.Direction.ASC, "name");
        Price price1 = entityManager.persist(new Price("BYN", 410));
        Price price2 = entityManager.persist(new Price("RUB", 410));
        Category category1 = entityManager.persist(new Category( "Category 1", null));
        Category category2 = entityManager.persist(new Category( "Category 2", null));
        product = entityManager.persist(new Product("Product", price1, category1));
        entityManager.persist(new Product(product.getName(), price2, category2));
        entityManager.persist(new Product(product.getName(), Arrays.asList(price1, price2), Arrays.asList(category1, category2)));
    }

    @After
    public void tearDown() {
        pageable = null;
        entityManager.clear();
    }


    @Test
    public void findAllByNameProductFound() {
        assertTrue(productDAO.findAllByName(product.getName(), pageable).contains(product));
    }

    @Test
    public void findAllByNameAllProductsFound() {
        assertEquals(3, productDAO.findAllByName(product.getName(), pageable).size());
    }

    @Test
    public void findAllByNameAbsent() {
        assertEquals(0, productDAO.findAllByName("Not a product", pageable).size());
    }

    @Test
    public void findAllByPriceProductFound() {
        assertTrue(productDAO.findAllByPrices(product.getPrices().get(0), pageable).contains(product));
    }

    @Test
    public void findAllByPriceAllProductsFound() {
        assertEquals(2, productDAO.findAllByPrices(product.getPrices().get(0), pageable).size());
    }

    @Test
    public void findAllByPriceAbsent() {
        assertEquals(0, productDAO.findAllByPrices(new Price(-1L, "", 0), pageable).size());
    }

    @Test
    public void findAllByCategoryProductFound() {
        assertTrue(productDAO.findAllByCategories(product.getCategories().get(0), pageable).contains(product));
    }

    @Test
    public void findAllByCategoryAllProductsFound() {
        assertEquals(2, productDAO.findAllByCategories(product.getCategories().get(0), pageable).size());
    }

    @Test
    public void findAllByCategoryAbsent() {
        assertEquals(0, productDAO.findAllByCategories(new Category(-1L, "", null), pageable).size());
    }
}