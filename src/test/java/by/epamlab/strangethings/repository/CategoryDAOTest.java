package by.epamlab.strangethings.repository;


import by.epamlab.strangethings.model.Category;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CategoryDAOTest {
    private Pageable pageable;

    @Autowired
    private CategoryDAO categoryDAO;

    @Autowired
    private TestEntityManager entityManager;

    private Category category1;
    private Category category2;

    @Before
    public void setUp() {
        pageable = PageRequest.of(0, 5, Sort.Direction.ASC, "name");
        String name1 = "Category 1";
        String name2 = "Category 2";
        category1 = entityManager.persist(new Category(name1, null));
        category2 = entityManager.persist(new Category(name2, null));
    }

    @After
    public void tearDown() {
        pageable = null;
        entityManager.clear();
    }

    @Test
    public void findAllByNameCategoryExists() {
       assertTrue(categoryDAO.findAllByName(category1.getName(), pageable).contains(category1));
    }

    @Test
    public void findAllByNameCategoryAbsent() {
        assertFalse(categoryDAO.findAllByName("not exist", pageable).contains(category1));
    }

    @Test
    public void findAllByNameCategoryNumberFoundCategories() {
        assertEquals(1, categoryDAO.findAllByName(category2.getName(), pageable).size());
    }
}