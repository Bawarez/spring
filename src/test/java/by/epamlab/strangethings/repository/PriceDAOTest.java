package by.epamlab.strangethings.repository;

import by.epamlab.strangethings.model.Price;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PriceDAOTest {
    private Pageable pageable;

    @Autowired
    private PriceDAO priceDAO;

    @Autowired
    private TestEntityManager entityManager;

    @Before
    public void setUp() {
        pageable = PageRequest.of(0, 10, Sort.Direction.ASC, "currency");
        for (int i = 1; i < 4; i++) {
            entityManager.persist(new Price("BYN", 30 * i));
            entityManager.persist(new Price("RUB", 30 * i));
        }
    }

    @After
    public void tearDown() {
        pageable = null;
        entityManager.clear();
    }

    @Test
    public void findByCurrencyPresent() {
        String currency = "BYN";
        assertEquals(3, priceDAO.findAllByCurrency(currency, pageable)
                .stream()
                .filter(p -> p.getCurrency().equals(currency))
                .count());
    }

    @Test
    public void findByCurrencyAbsent() {
        String currency = "USD";
        assertEquals(0, priceDAO.findAllByCurrency(currency, pageable)
                .stream()
                .filter(p -> p.getCurrency().equals(currency))
                .count());
    }

    @Test
    public void findByRangeAllInRange() {
        String currency = "RUB";
        int min = 30;
        int max = 90;
        assertEquals(3, priceDAO.findAllByValueBetweenAndCurrency(min, max, currency, pageable)
                .stream()
                .filter(p -> p.getCurrency().equals(currency) && p.getValue() >= min && p.getValue() <= max)
                .count());
    }


    @Test
    public void findByRangePartInRange() {
        String currency = "RUB";
        int min = 50;
        int max = 100;
        assertEquals(2, priceDAO.findAllByValueBetweenAndCurrency(min, max, currency, pageable)
                .stream()
                .filter(p -> p.getCurrency().equals(currency) && p.getValue() >= min && p.getValue() <= max)
                .count());
    }

    @Test
    public void findByRangeOutOfRangeAndPresentedCurrency() {
        String currency = "RUB";
        int min = 100;
        int max = 150;
        assertEquals(0, priceDAO.findAllByValueBetweenAndCurrency(min, max, currency, pageable)
                .stream()
                .filter(p -> p.getCurrency().equals(currency) && p.getValue() >= min && p.getValue() <= max)
                .count());
    }

    @Test
    public void findByRangeOutOfRangeAndAbsentedCurrency() {
        String currency = "USD";
        int min = 1;
        int max = 20;
        assertEquals(0, priceDAO.findAllByValueBetweenAndCurrency(min, max, currency, pageable)
                .stream()
                .filter(p -> p.getCurrency().equals(currency) && p.getValue() >= min && p.getValue() <= max)
                .count());
    }

    @Test
    public void findByRangeInRangeAndAbsentedCurrency() {
        String currency = "USD";
        int min = 30;
        int max = 90;
        assertEquals(0, priceDAO.findAllByValueBetweenAndCurrency(min, max, currency, pageable)
                .stream()
                .filter(p -> p.getCurrency().equals(currency) && p.getValue() >= min && p.getValue() <= max)
                .count());
    }
}