package by.epamlab.strangethings.facade;

import by.epamlab.strangethings.dto.PriceDTO;
import by.epamlab.strangethings.mapper.PriceMapper;
import by.epamlab.strangethings.model.Price;
import by.epamlab.strangethings.repository.PriceDAO;
import by.epamlab.strangethings.repository.ProductDAO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PriceFacadeTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private PriceDAO priceDAO;

    @Autowired
    private ProductDAO productDAO;

    private Pageable pageable;
    private PriceFacade priceFacade;
    private PriceMapper priceMapper;
    private PriceDTO priceDTO;

    @Before
    public void setUp() {
        pageable = PageRequest.of(0, 10, Sort.Direction.ASC, "currency");
        priceMapper = new PriceMapper();
        priceFacade = new PriceFacade(priceDAO, priceMapper, productDAO);

        Price price = null;
        for (int i = 1; i < 4; i++) {
            entityManager.persist(new Price("RUB", 30 * i));
            price = entityManager.persist(new Price("BYN", 30 * i));
        }
        priceDTO = priceMapper.mapToDTO(price);
    }

    @After
    public void tearDown() {
        entityManager.clear();
    }

    @Test
    public void getExisted() {
        assertEquals(priceDTO, priceFacade.get(priceDTO.getId()));
    }

    @Test
    public void getNotExisted() {
        PriceDTO priceDTO = priceMapper.mapToDTO(new Price(-1L, "", 0));
        assertNull(priceFacade.get(priceDTO.getId()));
    }

    @Test
    public void getAllCorrectNumber() {
        assertEquals(6, priceFacade.getAll(pageable).size());
    }

    @Test
    public void saveEquals() {
        PriceDTO price = priceMapper.mapToDTO(new Price("BYN", 100));
        PriceDTO savedPrice = priceFacade.create(price);
        price.setId(savedPrice.getId());
        assertEquals(price, savedPrice);
    }

    @Test(expected = org.springframework.dao.DataIntegrityViolationException.class)
    public void saveInvalid() {
        PriceDTO price = priceMapper.mapToDTO(new Price(null, 100));
        priceFacade.create(price);
    }

    @Test
    public void updateEquals() {
        priceDTO.setValue(150);
        PriceDTO updatedPrice = priceFacade.update(priceDTO);
        assertEquals(priceDTO, updatedPrice);
    }

    @Test
    public void updateNotEquals() {
        PriceDTO oldPrice = priceMapper.mapToDTO(priceMapper.mapToModel(priceDTO));
        priceDTO.setValue(150);
        PriceDTO updatedPrice = priceFacade.update(priceDTO);
        assertNotEquals(oldPrice, updatedPrice);
    }

    @Test
    public void updateNotExistedNullReturned() {
        priceDTO.setId(priceDTO.getId() + 10);
        assertNull(priceFacade.update(priceDTO));
    }

    @Test
    public void updateNotExistedPricesNumberNotChanged() {
        int expectedPricesNumber = priceFacade.getAll(pageable).size();
        priceDTO.setId(priceDTO.getId() + 10);
        priceFacade.update(priceDTO);
        assertEquals(expectedPricesNumber, priceFacade.getAll(pageable).size());
    }

    @Test
    public void updateExistedPricesNumberNotChanged() {
        int expectedPricesNumber = priceFacade.getAll(pageable).size();
        priceFacade.update(priceDTO);
        assertEquals(expectedPricesNumber, priceFacade.getAll(pageable).size());
    }

    @Test
    public void deleteDeletedPriceAbsent() {
        priceFacade.delete(priceDTO.getId());
        assertFalse(priceFacade.getAll(pageable).contains(priceDTO));
    }

    @Test
    public void deletePricesNumberReduced() {
        int expectedPricesNumber = priceFacade.getAll(pageable).size() - 1;
        priceFacade.delete(priceDTO.getId());
        assertEquals(expectedPricesNumber, priceFacade.getAll(pageable).size());
    }

    @Test
    public void deleteAbsent() {
        int expectedPricesNumber = priceFacade.getAll(pageable).size();
        Long id = priceDTO.getId() + 10;
        priceFacade.delete(id);
        assertEquals(expectedPricesNumber, priceFacade.getAll(pageable).size());
    }

    @Test
    public void findByCurrencyPresent() {
        String currency = "BYN";
        assertEquals(3, priceFacade.findByCurrency(currency, pageable)
                .stream()
                .filter(p -> p.getCurrency().equals(currency))
                .count());
    }

    @Test
    public void findByCurrencyAbsent() {
        String currency = "USD";
        assertEquals(0, priceFacade.findByCurrency(currency, pageable)
                .stream()
                .filter(p -> p.getCurrency().equals(currency))
                .count());
    }

    @Test
    public void findByRangeAllInRange() {
        String currency = "RUB";
        int min = 30;
        int max = 90;
        System.out.println(pageable.getPageSize() + " " + pageable.getSort());
        assertEquals(3, priceFacade.findByRange(min, max, currency, pageable)
                .stream()
                .filter(p -> p.getCurrency().equals(currency) && p.getValue() >= min && p.getValue() <= max)
                .count());
    }

    @Test
    public void findByRangePartInRange() {
        String currency = "RUB";
        int min = 50;
        int max = 100;
        assertEquals(2, priceFacade.findByRange(min, max, currency, pageable)
                .stream()
                .filter(p -> p.getCurrency().equals(currency) && p.getValue() >= min && p.getValue() <= max)
                .count());
    }

    @Test
    public void findByRangeOutOfRangeAndPresentedCurrency() {
        String currency = "RUB";
        int min = 100;
        int max = 150;
        assertEquals(0, priceFacade.findByRange(min, max, currency, pageable)
                .stream()
                .filter(p -> p.getCurrency().equals(currency) && p.getValue() >= min && p.getValue() <= max)
                .count());
    }

    @Test
    public void findByRangeOutOfRangeAndAbsentedCurrency() {
        String currency = "USD";
        int min = 1;
        int max = 20;
        assertEquals(0, priceFacade.findByRange(min, max, currency, pageable)
                .stream()
                .filter(p -> p.getCurrency().equals(currency) && p.getValue() >= min && p.getValue() <= max)
                .count());
    }

    @Test
    public void findByRangeInRangeAndAbsentedCurrency() {
        String currency = "USD";
        int min = 30;
        int max = 90;
        assertEquals(0, priceFacade.findByRange(min, max, currency, pageable)
                .stream()
                .filter(p -> p.getCurrency().equals(currency) && p.getValue() >= min && p.getValue() <= max)
                .count());
    }
}