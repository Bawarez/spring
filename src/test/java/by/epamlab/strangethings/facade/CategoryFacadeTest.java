package by.epamlab.strangethings.facade;

import by.epamlab.strangethings.dto.CategoryDTO;
import by.epamlab.strangethings.mapper.CategoryMapper;
import by.epamlab.strangethings.model.Category;
import by.epamlab.strangethings.repository.CategoryDAO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CategoryFacadeTest {
    @Autowired
    private CategoryDAO categoryDAO;

    @Autowired
    private TestEntityManager entityManager;

    private CategoryMapper categoryMapper;
    private CategoryFacade categoryFacade;
    private Pageable pageable;
    private CategoryDTO categoryDTO;

    @Before
    public void setUp() {
        pageable = PageRequest.of(0, 5, Sort.Direction.ASC, "name");
        categoryMapper = new CategoryMapper(categoryDAO);
        categoryFacade = new CategoryFacade(categoryDAO, categoryMapper);

        Category category = entityManager.persist(new Category("Supercategory", null));
        category = entityManager.persist(new Category("Subcategory", category));
        categoryDTO = categoryMapper.mapToDTO(category);
    }

    @After
    public void tearDown() {
        pageable = null;
        entityManager.clear();
    }

    @Test
    public void getExisted() {
        assertEquals(categoryDTO, categoryFacade.get(categoryDTO.getId()));
    }

    @Test
    public void getNotExisted() {
        assertNull(categoryFacade.get(categoryDTO.getId() + 10));
    }

    @Test
    public void getAllCorrectNumber() {
        assertEquals(2, categoryFacade.getAll(pageable).size());
    }

    @Test
    public void saveEquals() {
        CategoryDTO category = categoryMapper.mapToDTO(new Category("New category", null));
        CategoryDTO savedCategory = categoryFacade.create(category);
        category.setId(savedCategory.getId());
        assertEquals(category, savedCategory);
    }

    @Test(expected = org.springframework.dao.DataIntegrityViolationException.class)
    public void saveInvalid() {
        CategoryDTO category = categoryMapper.mapToDTO(new Category(null, null));
        categoryFacade.create(category);
    }

    @Test
    public void updateEquals() {
        categoryDTO.setName("New category");
        CategoryDTO updatedCategory = categoryFacade.update(categoryDTO);
        assertEquals(categoryDTO, updatedCategory);
    }

    @Test
    public void updateNotEquals() {
        CategoryDTO oldCategory = categoryMapper.mapToDTO(categoryMapper.mapToModel(categoryDTO));
        categoryDTO.setName("New category");
        CategoryDTO updatedCategory = categoryFacade.update(categoryDTO);
        assertNotEquals(oldCategory, updatedCategory);
    }

    @Test
    public void updateNotExistedNullReturned() {
        categoryDTO.setId(categoryDTO.getId() + 10);
        assertNull(categoryFacade.update(categoryDTO));
    }

    @Test
    public void updateNotExistedCategoriesNumberNotChanged() {
        int expectedPricesNumber = categoryFacade.getAll(pageable).size();
        categoryDTO.setId(categoryDTO.getId() + 10);
        categoryFacade.update(categoryDTO);
        assertEquals(expectedPricesNumber, categoryFacade.getAll(pageable).size());
    }

    @Test
    public void updateExistedCategoriesNumberNotChanged() {
        int expectedCategoriesNumber = categoryFacade.getAll(pageable).size();
        categoryFacade.update(categoryDTO);
        assertEquals(expectedCategoriesNumber, categoryFacade.getAll(pageable).size());
    }

    @Test
    public void deleteDeletedCategoryAbsent() {
        categoryFacade.delete(categoryDTO.getId());
        assertFalse(categoryFacade.getAll(pageable).contains(categoryDTO));
    }

    @Test
    public void deleteCategoriesNumberReduced() {
        int expectedCategoriesNumber = categoryFacade.getAll(pageable).size() - 1;
        categoryFacade.delete(categoryDTO.getId());
        assertEquals(expectedCategoriesNumber, categoryFacade.getAll(pageable).size());
    }

    @Test
    public void deleteAbsent() {
        int expectedCategoriesNumber = categoryFacade.getAll(pageable).size();
        Long id = categoryDTO.getId() + 10;
        categoryFacade.delete(id);
        assertEquals(expectedCategoriesNumber, categoryFacade.getAll(pageable).size());
    }

    @Test
    public void findByNameCategoryExists() {
        assertTrue(categoryFacade.findByName(categoryDTO.getName(), pageable).contains(categoryDTO));
    }

    @Test
    public void findByNameCategoryAbsent() {
        assertFalse(categoryFacade.findByName("not exist", pageable).contains(categoryDTO));
    }

    @Test
    public void findByNameCategoryNumberFoundCategories() {
        assertEquals(1, categoryFacade.findByName(categoryDTO.getName(), pageable).size());
    }
}
