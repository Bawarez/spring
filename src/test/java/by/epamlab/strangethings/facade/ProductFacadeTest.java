package by.epamlab.strangethings.facade;

import by.epamlab.strangethings.dto.ProductDTO;
import by.epamlab.strangethings.mapper.ProductMapper;
import by.epamlab.strangethings.model.Category;
import by.epamlab.strangethings.model.Price;
import by.epamlab.strangethings.model.Product;
import by.epamlab.strangethings.repository.CategoryDAO;
import by.epamlab.strangethings.repository.PriceDAO;
import by.epamlab.strangethings.repository.ProductDAO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProductFacadeTest {
    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private PriceDAO priceDAO;

    @Autowired
    private CategoryDAO categoryDAO;

    @Autowired
    private TestEntityManager entityManager;

    private Pageable pageable;
    private ProductFacade productFacade;
    private ProductMapper productMapper;
    private ProductDTO productDTO;

    @Before
    public void setUp() {
        pageable = PageRequest.of(0, 5, Sort.Direction.ASC, "name");
        productMapper = new ProductMapper(priceDAO, categoryDAO);
        productFacade = new ProductFacade(productDAO, productMapper);

        Price price1 = entityManager.persist(new Price("BYN", 410));
        Price price2 = entityManager.persist(new Price("RUB", 410));
        Category category1 = entityManager.persist(new Category( "Category 1", null));
        Category category2 = entityManager.persist(new Category( "Category 2", null));
        Product product = entityManager.persist(new Product("Product", price1, category1));
        entityManager.persist(new Product(product.getName(), price2, category2));
        entityManager.persist(new Product(product.getName(), Arrays.asList(price1, price2), Arrays.asList(category1, category2)));

        productDTO = productMapper.mapToDTO(product);
    }

    @After
    public void tearDown() {
        pageable = null;
        entityManager.clear();
    }


    @Test
    public void getExisted() {
        assertEquals(productDTO, productFacade.get(productDTO.getId()));
    }

    @Test
    public void getNotExisted() {
        assertNull(productFacade.get(productDTO.getId() + 10));
    }

    @Test
    public void getAllCorrectNumber() {
        assertEquals(3, productFacade.getAll(pageable).size());
    }

    @Test
    public void saveEquals() {
        Price price = entityManager.persist(new Price("BYN", 100));
        Category category = entityManager.persist(new Category("New category", null));
        ProductDTO product = productMapper.mapToDTO(new Product("New product", price, category));
        ProductDTO savedProduct = productFacade.create(product);
        product.setId(savedProduct.getId());
        assertEquals(product, savedProduct);
    }

    @Test(expected = org.springframework.dao.DataIntegrityViolationException.class)
    public void saveInvalid() {
        Price price = entityManager.persist(new Price("BYN", 100));
        ProductDTO product = productMapper.mapToDTO(new Product(null, price, new Category()));
        productFacade.create(product);
    }

    @Test
    public void updateEquals() {
        productDTO.setName("New product");
        ProductDTO updatedProduct = productFacade.update(productDTO);
        assertEquals(productDTO, updatedProduct);
    }

    @Test
    public void updateNotEquals() {
        ProductDTO oldProduct = productMapper.mapToDTO(productMapper.mapToModel(productDTO));
        productDTO.setName("New product");
        ProductDTO updatedProduct = productFacade.update(productDTO);
        assertNotEquals(oldProduct, updatedProduct);
    }

    @Test
    public void updateNotExistedNullReturned() {
        productDTO.setId(productDTO.getId() + 10);
        assertNull(productFacade.update(productDTO));
    }

    @Test
    public void updateNotExistedProductsNumberNotChanged() {
        int expectedProductsNumber = productFacade.getAll(pageable).size();
        productDTO.setId(productDTO.getId() + 10);
        productFacade.update(productDTO);
        assertEquals(expectedProductsNumber, productFacade.getAll(pageable).size());
    }

    @Test
    public void updateExistedProductsNumberNotChanged() {
        int expectedProductsNumber = productFacade.getAll(pageable).size();
        productFacade.update(productDTO);
        assertEquals(expectedProductsNumber, productFacade.getAll(pageable).size());
    }

    @Test
    public void deleteDeletedProductAbsent() {
        productFacade.delete(productDTO.getId());
        assertFalse(productFacade.getAll(pageable).contains(productDTO));
    }

    @Test
    public void deleteProductsNumberReduced() {
        int expectedProductsNumber = productFacade.getAll(pageable).size() - 1;
        productFacade.delete(productDTO.getId());
        assertEquals(expectedProductsNumber, productFacade.getAll(pageable).size());
    }

    @Test
    public void deleteAbsent() {
        int expectedProductsNumber = productFacade.getAll(pageable).size();
        Long id = productDTO.getId() + 10;
        productFacade.delete(id);
        assertEquals(expectedProductsNumber, productFacade.getAll(pageable).size());
    }

    @Test
    public void findByNameProductFound() {
        assertTrue(productFacade.findByName(productDTO.getName(), pageable).contains(productDTO));
    }

    @Test
    public void findByNameAllProductsFound() {
        assertEquals(3, productFacade.findByName(productDTO.getName(), pageable).size());
    }

    @Test
    public void findByNameAbsent() {
        assertEquals(0, productFacade.findByName("Not a product", pageable).size());
    }

    @Test
    public void findByPriceProductFound() {
        assertTrue(productFacade.findByPrice(productDTO.getPrices().get(0), pageable).contains(productDTO));
    }

    @Test
    public void findByPriceAllProductsFound() {
        assertEquals(2, productFacade.findByPrice(productDTO.getPrices().get(0), pageable).size());
    }

    @Test
    public void findByPriceAbsent() {
        assertEquals(0, productFacade.findByPrice(productDTO.getPrices().get(0) + 10, pageable).size());
    }

    @Test
    public void findByCategoryProductFound() {
        assertTrue(productFacade.findByCategory(productDTO.getCategories().get(0), pageable).contains(productDTO));
    }

    @Test
    public void findByCategoryAllProductsFound() {
        assertEquals(2, productFacade.findByCategory(productDTO.getCategories().get(0), pageable).size());
    }

    @Test
    public void findByCategoryAbsent() {
        assertEquals(0, productFacade.findByCategory(productDTO.getPrices().get(0) + 10, pageable).size());
    }
}