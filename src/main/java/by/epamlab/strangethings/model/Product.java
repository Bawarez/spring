package by.epamlab.strangethings.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Represents product
 */
@Data
@ToString(exclude = {"prices", "categories"})
@NoArgsConstructor
@Entity(name = "Products")
public class Product implements Model {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @ManyToMany
    @Column(nullable = false)
    private List<Price> prices = new ArrayList<>();

    @ManyToMany
    private List<Category> categories = new ArrayList<>();

    /**
     * Constructs a product with the specified id, name, list of prices, list of categories
     * @param id product id
     * @param name product name
     * @param prices prices list for this product
     * @param categories categories that this product belongs to
     */
    public Product(final Long id, final String name, final List<Price> prices, final List<Category> categories) {
        this.id = id;
        this.name = name;
        this.prices = prices;
        this.categories = categories;
    }

    /**
     * Constructs a product with the specified name, list of prices, list of categories
     * @param name product name
     * @param prices prices list for this product
     * @param categories categories that this product belongs to
     */
    public Product(final String name, final List<Price> prices, final List<Category> categories) {
        this(null, name, prices, categories);
    }


    /**
     * Constructs a product with the specified id, name, price, category
     * @param name product name
     * @param price price for this product
     * @param category category that this product belongs to
     */
    public Product(final String name, final Price price, final Category category) {
        this.name = name;
        this.prices.add(price);
        this.categories.add(category);
    }
}
