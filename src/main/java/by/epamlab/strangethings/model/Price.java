package by.epamlab.strangethings.model;


import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;


/**
 * Represents price
 */
@Data
@ToString(exclude = "id")
@NoArgsConstructor
@Entity(name = "Prices")
public class Price implements Model {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String currency;

    private int value;

    /**
     * Constructs a price with the specified id, currency, value
     * @param id price id
     * @param currency price currency
     * @param value price value
     */
    public Price(final Long id, final String currency, final int value) {
        this(currency, value);
        this.id = id;
    }

    /**
     * Constructs a price with the specified currency, value
     * @param currency price currency
     * @param value price value
     */
    public Price(final String currency, final int value) {
        this.currency = currency;
        this.value = value;
    }
}
