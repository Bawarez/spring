package by.epamlab.strangethings.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


/**
 * Represents category
 */
@Data
@NoArgsConstructor
@Entity(name = "Categories")
public class Category implements Model {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @OneToOne
    private Category superCategory;

    /**
     * Constructs a category with the specified id, name and super category
     * @param id category id
     * @param name category name
     * @param superCategory parent category
     */
    public Category(final Long id, final String name, final Category superCategory) {
        this(name, superCategory);
        this.id = id;
    }

    /**
     * Constructs a category with the name and super category
     * @param name category name
     * @param superCategory parent category
     */
    public Category(final String name, final Category superCategory) {
        this.name = name;
        this.superCategory = superCategory;
    }
}
