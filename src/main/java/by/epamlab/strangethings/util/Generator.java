package by.epamlab.strangethings.util;

import org.springframework.stereotype.Component;

import java.util.Random;

/**
 * Provides methods to generate random strings and int values
 */
@Component
public class Generator {
    private Random rnd = new Random();
    private String[] products = ("Elevator;Controller;Passenger;Tomato;Strange behavior;Empty target;"
            + "Something complicated;Doc comment;Stream;Rubber duck").split(";");
    private int currentProduct = -1;

    /**
     * Generates a random string of {@code length} length
     * @param length generated string length
     * @return random string
     */
    public String generateString(final int length) {
        if (length == 7 && (rnd.nextInt(10) > 6)) {
            currentProduct = ++currentProduct % products.length;
            return products[currentProduct];
        }

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            char c = (char) (rnd.nextInt('z' - 'a') + 'a');
            sb.append(c);
        }
        return sb.toString().intern();
    }

    /**
     * Generates a random int value in range from {@code min} to {@code max} (exclusive)
     * @param min minimum possible value
     * @param max the upper bound (exclusive)
     * @return a random int value in range from {@code min} to {@code max} (exclusive)
     */
    public int generateInt(final int min, final int max) {
        return min + rnd.nextInt(max - min);
    }
}
