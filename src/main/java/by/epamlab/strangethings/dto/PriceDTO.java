package by.epamlab.strangethings.dto;

import lombok.Data;

/**
 * Price data transfer object
 */
@Data
public class PriceDTO implements DTO<Long> {
    private Long id;
    private String currency;
    private int value;
}
