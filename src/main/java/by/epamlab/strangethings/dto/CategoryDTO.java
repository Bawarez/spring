package by.epamlab.strangethings.dto;


import lombok.Data;

/**
 * Category data transfer object
 */
@Data
public class CategoryDTO implements DTO<Long> {
    private Long id;
    private String name;
    private Long superCategory;
}
