package by.epamlab.strangethings.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Product data transfer object
 */
@Data
public class ProductDTO implements DTO<Long> {
    private Long id;
    private String name;
    private List<Long> prices = new ArrayList<>();
    private List<Long> categories = new ArrayList<>();
}
