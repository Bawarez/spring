package by.epamlab.strangethings.dto;

/**
 * Declares data transfer object methods
 */
public interface DTO<ID> {
    ID getId();
}
