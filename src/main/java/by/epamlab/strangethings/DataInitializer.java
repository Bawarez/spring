package by.epamlab.strangethings;

import by.epamlab.strangethings.model.Category;
import by.epamlab.strangethings.model.Price;
import by.epamlab.strangethings.model.Product;
import by.epamlab.strangethings.repository.CategoryDAO;
import by.epamlab.strangethings.repository.PriceDAO;
import by.epamlab.strangethings.repository.ProductDAO;
import by.epamlab.strangethings.util.Generator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * Fills database with random data after application starting
 */
@Component
public class DataInitializer implements ApplicationRunner {
    private final ProductDAO productDAO;
    private final CategoryDAO categoryDAO;
    private final PriceDAO priceDAO;
    private final Generator generator;

    /**
     * Constructs an instance with specified product DAO, category DAO, price DAO, generator
     * @param productDAO product DAO
     * @param categoryDAO category DAO
     * @param priceDAO price DAO
     * @param generator generator
     */
    @Autowired
    public DataInitializer(final ProductDAO productDAO,
                           final CategoryDAO categoryDAO,
                           final PriceDAO priceDAO,
                           final Generator generator) {
        this.productDAO = productDAO;
        this.categoryDAO = categoryDAO;
        this.priceDAO = priceDAO;
        this.generator = generator;
    }

    /**
     * Fills database with random data
     * @param args application args
     */
    @Override
    public void run(final ApplicationArguments args) {
        final int pricesNumber = 15;
        final int categoriesNumber = 100;
        final int productsNumber = 100;

        Category rootCategory = categoryDAO.save(new Category("Zakat", null));
        Category[] categories = new Category[categoriesNumber];
        for (int i = 0; i < categories.length; i++) {
            String name = generator.generateString(10);
            Category superCategory = rootCategory;
            if ((i % 5) > 2) {
                superCategory = categories[i - 1];
            }
            categories[i] = categoryDAO.save(new Category(name, superCategory));
        }

        Price testPrice = priceDAO.save(new Price("BYN", 410));
        Price[] prices = new Price[pricesNumber];
        for (int i = 0; i < prices.length; i++) {
            prices[i] = generatePrice(i);
        }


        productDAO.save(new Product("Test", testPrice, rootCategory));
        for (int i = 0; i < productsNumber; i++) {
            Price price0 = prices[i % prices.length];
            Price price1 = generatePrice(i);
            String name = generator.generateString(7);
            Category category = categories[i % categoriesNumber];
            productDAO.save(new Product(name, Arrays.asList(price0, price1), Arrays.asList(category)));
        }
    }

    private Price generatePrice(final int i) {
        final int minPrice = 1;
        final int maxPrice = 666;
        final String[] currencies = {"BYN", "RUB", "USD", "EUR"};

        String currency = currencies[i % currencies.length];
        int value = generator.generateInt(minPrice, maxPrice);

        return priceDAO.save(new Price(currency, value));
    }
}

