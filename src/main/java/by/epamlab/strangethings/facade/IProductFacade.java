package by.epamlab.strangethings.facade;

import by.epamlab.strangethings.dto.ProductDTO;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Declares product facade methods
 */
public interface IProductFacade extends Facade<ProductDTO, Long> {
    List<ProductDTO> findByName(String name, Pageable pageable);
    List<ProductDTO> findByPrice(Long priceId, Pageable pageable);
    List<ProductDTO> findByCategory(Long categoryId, Pageable pageable);
}
