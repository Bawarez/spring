package by.epamlab.strangethings.facade;

import by.epamlab.strangethings.dto.PriceDTO;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Declares price facade methods
 */
public interface IPriceFacade extends Facade<PriceDTO, Long> {
    List<PriceDTO> findByCurrency(String currency, Pageable pageable);
    List<PriceDTO> findByProduct(Long productId);
    List<PriceDTO> findByRange(int min, int max, String currency, Pageable pageable);
}
