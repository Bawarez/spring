package by.epamlab.strangethings.facade;

import by.epamlab.strangethings.dto.CategoryDTO;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Declares category facade methods
 */
public interface ICategoryFacade extends Facade<CategoryDTO, Long> {
    List<CategoryDTO> findByName(String name, Pageable pageable);
}
