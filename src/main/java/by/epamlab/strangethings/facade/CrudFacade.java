package by.epamlab.strangethings.facade;

import by.epamlab.strangethings.model.Model;
import by.epamlab.strangethings.repository.ModelDAO;
import by.epamlab.strangethings.mapper.EntityMapper;

import by.epamlab.strangethings.dto.DTO;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Provides methods for CRUD operations
 * @param <M> model type
 * @param <D> DTO type
 * @param <ID> ID field type
 */
public abstract class CrudFacade<M extends Model, D extends DTO<ID>, ID>  implements Facade<D, ID> {
    protected final EntityMapper<M, D, ID> mapper;
    protected final ModelDAO<M, ID> dao;

    protected CrudFacade(final ModelDAO<M, ID> dao, final EntityMapper<M, D, ID> mapper) {
        this.mapper = mapper;
        this.dao = dao;
    }

    /**
     * Returns entity DTO by specified id
     * @param id ID
     * @return entity DTO
     */
    public D get(final ID id) {
        M model = dao.findById(id)
                .orElse(null);
        if (model == null) {
            return null;
        }
        return mapper.mapToDTO(model);
    }

    /**
     * Returns all present entity DTO
     * @param pageable pageable
     * @return found DTO list
     */
    public List<D> getAll(final Pageable pageable) {
        return mapper.mapAllToDTO(dao.findAll(pageable).getContent());
    }

    /**
     * Adds entity according specified DTO
     * @param dto entity DTO
     * @return added entity DTO
     */
    public D create(final D dto) {
        if (dto.getId() != null && !dao.existsById(dto.getId())) {
            return null;
        }
        M model = dao.save(mapper.mapToModel(dto));
        return mapper.mapToDTO(model);
    }

    /**
     * Updates existing entity according specified DTO
     * @param dto entity DTO to update
     * @return updated entity DTO
     */
    public D update(final D dto) {
        if (!dao.existsById(dto.getId())) {
            return null;
        }
        M model = dao.save(mapper.mapToModel(dto));
        return mapper.mapToDTO(model);
    }

    /**
     * Removes entity with specified id
     * @param id entity id
     */
    public void delete(final ID id) {
        if (dao.existsById(id)) {
            dao.deleteById(id);
        }
    }
}
