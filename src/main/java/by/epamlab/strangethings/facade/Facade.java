package by.epamlab.strangethings.facade;

import by.epamlab.strangethings.dto.DTO;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Declares common facade methods
 * @param <D> DTO type
 * @param <ID> ID field type
 */
public interface Facade<D extends DTO<ID>, ID> {
    D get(ID id);
    List<D> getAll(Pageable pageable);
    D create(D dto);
    D update(D dto);
    void delete(ID id);
}
