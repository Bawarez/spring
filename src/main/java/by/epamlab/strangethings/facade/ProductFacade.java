package by.epamlab.strangethings.facade;

import by.epamlab.strangethings.dto.ProductDTO;
import by.epamlab.strangethings.model.Category;
import by.epamlab.strangethings.model.Price;
import by.epamlab.strangethings.model.Product;
import by.epamlab.strangethings.repository.ProductDAO;
import by.epamlab.strangethings.mapper.EntityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Provides operations over products
 */
@Component
public class ProductFacade extends CrudFacade<Product, ProductDTO, Long> implements IProductFacade {

    /**
     * Constructs an instance with the specified product DAO, mapper for products,
     * @param productDAO product DAO
     * @param mapper mapper converting product model to DTO and vice versa
     */
    @Autowired
    protected ProductFacade(final ProductDAO productDAO,
                            final EntityMapper<Product, ProductDTO, Long> mapper) {
        super(productDAO, mapper);
    }

    /**
     * Search products with the specified name
     * @param name name for search
     * @param pageable pageable
     * @return list of found products
     */
    public List<ProductDTO> findByName(final String name, final Pageable pageable) {
        return mapper.mapAllToDTO(
                ((ProductDAO) dao).findAllByName(name, pageable)
        );
    }

    /**
     * Search products by the specified price id
     * @param priceId price id
     * @param pageable pageable
     * @return list of found products
     */
    public List<ProductDTO> findByPrice(final Long priceId, final Pageable pageable) {
        Price price = new Price();
        price.setId(priceId);

        return mapper.mapAllToDTO(
                ((ProductDAO) dao).findAllByPrices(price, pageable)
        );
    }

    /**
     * Search products by the specified category id
     * @param categoryId category id
     * @param pageable pageable
     * @return list of found products
     */
    public List<ProductDTO> findByCategory(final Long categoryId, final Pageable pageable) {
        Category category = new Category();
        category.setId(categoryId);

        return mapper.mapAllToDTO(
                ((ProductDAO) dao).findAllByCategories(category, pageable)
        );
    }
}
