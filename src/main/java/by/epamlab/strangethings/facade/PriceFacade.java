package by.epamlab.strangethings.facade;

import by.epamlab.strangethings.model.Price;
import by.epamlab.strangethings.model.Product;
import by.epamlab.strangethings.repository.PriceDAO;
import by.epamlab.strangethings.repository.ProductDAO;
import by.epamlab.strangethings.mapper.EntityMapper;
import by.epamlab.strangethings.dto.PriceDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Provides operations over prices
 */
@Component
public class PriceFacade extends CrudFacade<Price, PriceDTO, Long> implements IPriceFacade {
    private final ProductDAO productDAO;

    /**
     * Constructs an instance with the specified price DAO, mapper for prices, product DAO
     * @param priceDAO price DAO
     * @param mapper mapper converting price model to DTO and vice versa
     * @param productDAO product DAO
     */
    @Autowired
    public PriceFacade(final PriceDAO priceDAO,
                       final EntityMapper<Price, PriceDTO, Long> mapper,
                       final ProductDAO productDAO) {
        super(priceDAO, mapper);
        this.productDAO = productDAO;
    }

    /**
     * Search prices with specified currency
     * @param currency currency for search
     * @param pageable pageable
     * @return list of found prices
     */
    public List<PriceDTO> findByCurrency(final String currency, final Pageable pageable) {
        return mapper.mapAllToDTO(
                ((PriceDAO) dao).findAllByCurrency(currency, pageable)
        );
    }

    /**
     * Search prices by specified product id
     * @param productId product id for search
     * @return list of found prices
     */
    public List<PriceDTO> findByProduct(final Long productId) {
        List<Price> prices = productDAO.findById(productId)
                .map(Product::getPrices)
                .orElseGet(ArrayList::new);
        return mapper.mapAllToDTO(prices);
    }

    /**
     * Search prices with the specified currency and value in range from min to max
     * @param min minimal price value
     * @param max maximal price value
     * @param currency currency for search
     * @param pageable pageable
     * @return list of found prices
     */
    public List<PriceDTO> findByRange(final int min, final int max, final String currency, final Pageable pageable) {
        return mapper.mapAllToDTO(
                ((PriceDAO) dao).findAllByValueBetweenAndCurrency(min, max, currency, pageable)
        );
    }
}
