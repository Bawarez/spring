package by.epamlab.strangethings.facade;

import by.epamlab.strangethings.model.Category;
import by.epamlab.strangethings.repository.CategoryDAO;
import by.epamlab.strangethings.mapper.EntityMapper;
import by.epamlab.strangethings.dto.CategoryDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Provides operations over categories
 */
@Component
public class CategoryFacade extends CrudFacade<Category, CategoryDTO, Long> implements ICategoryFacade {

    /**
     * Constructs an instance with specified category DAO and mapper for categories
     * @param categoryDAO category DAO
     * @param mapper mapper converting category model to DTO and vice versa
     */
    @Autowired
    public CategoryFacade(final CategoryDAO categoryDAO,
                          final EntityMapper<Category, CategoryDTO, Long> mapper) {
        super(categoryDAO, mapper);
    }

    /**
     * Search categories with specified name
     * @param name category name for search
     * @param pageable pageable
     * @return list of found categories
     */
    public List<CategoryDTO> findByName(final String name, final Pageable pageable) {
        return mapper.mapAllToDTO(
                ((CategoryDAO) dao).findAllByName(name, pageable)
        );
    }
}
