package by.epamlab.strangethings.controller;

import by.epamlab.strangethings.dto.PriceDTO;
import by.epamlab.strangethings.facade.IPriceFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Provides operations over prices
 */
@RestController
@RequestMapping("/prices")
public class PriceController extends CrudController<PriceDTO, Long> {

    /**
     * Constructs an instance with the specified price facade
     * @param priceFacade price facade
     */
    @Autowired
    public PriceController(final IPriceFacade priceFacade) {
        super(priceFacade);
    }

    /**
     * Search prices by specified product id
     * @param product product id for search
     * @return list of found prices
     */
    @GetMapping("/search")
    public ResponseEntity<List<PriceDTO>> findByProduct(@RequestParam final Long product) {
        return ResponseEntity.ok(
                ((IPriceFacade) facade).findByProduct(product)
        );
    }

    /**
     * Search prices with specified currency or by value range if min and max params passed
     * @param currency currency for search
     * @param min minimal price value
     * @param max maximal price value
     * @param pageable pageable
     * @return list of found prices
     */
    @GetMapping("/search/{currency}")
    public ResponseEntity<List<PriceDTO>> findPrices(@PathVariable final String currency,
                                                     @RequestParam(required = false, defaultValue = "-1") final int min,
                                                     @RequestParam(required = false, defaultValue = "-1") final int max,
                                                     final Pageable pageable) {
        List<PriceDTO> prices;
                if (min == -1) {
                    prices = ((IPriceFacade) facade).findByCurrency(currency, pageable);
                } else {
                    prices = ((IPriceFacade) facade).findByRange(min, max, currency, pageable);
                }
        return ResponseEntity.ok(prices);
    }
}
