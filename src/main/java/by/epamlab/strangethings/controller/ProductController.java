package by.epamlab.strangethings.controller;

import by.epamlab.strangethings.dto.ProductDTO;
import by.epamlab.strangethings.facade.IProductFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Provides operations over products
 */
@RestController
@RequestMapping("/products")
public class ProductController extends CrudController<ProductDTO, Long> {

    /**
     * Constructs an instance with the specified product facade
     * @param productFacade product facade
     */
    @Autowired
    public ProductController(final IProductFacade productFacade) {
        super(productFacade);
    }

    /**
     * Search products with the specified name
     * @param name name for search
     * @param pageable pageable
     * @return list of found products
     */
    @GetMapping("/search/name")
    public ResponseEntity<List<ProductDTO>> findByName(@RequestParam final String name, final Pageable pageable) {
        return ResponseEntity.ok(
                ((IProductFacade) facade).findByName(name, pageable)
        );
    }

    /**
     * Search products by the specified category id
     * @param id category id
     * @param pageable pageable
     * @return list of found products
     */
    @GetMapping("/search/category")
    public ResponseEntity<List<ProductDTO>> findByCategory(@RequestParam final Long id, final Pageable pageable) {
        return ResponseEntity.ok(
                ((IProductFacade) facade).findByCategory(id, pageable)
        );
    }

    /**
     * Search products by the specified price id
     * @param id price id
     * @param pageable pageable
     * @return list of found products
     */
    @GetMapping("/search/price")
    public ResponseEntity<List<ProductDTO>> findByPrice(@RequestParam final Long id, final Pageable pageable) {
        return ResponseEntity.ok(((IProductFacade) facade).findByPrice(id, pageable));
    }
}
