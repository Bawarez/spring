package by.epamlab.strangethings.controller;

import by.epamlab.strangethings.dto.CategoryDTO;
import by.epamlab.strangethings.facade.ICategoryFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Provides operations over categories
 */
@RestController
@RequestMapping("/categories")
public class CategoryController extends CrudController<CategoryDTO, Long> {
    /**
     * Constructs an instance with the specified category facade
     * @param facade category facade
     */
    @Autowired
    public CategoryController(final ICategoryFacade facade) {
        super(facade);
    }

    /**
     * Search categories with specified name
     * @param name category name for search
     * @param pageable pageable
     * @return found categories
     */
    @GetMapping("/search")
    public ResponseEntity<List<CategoryDTO>> findCategories(@RequestParam final String name, final Pageable pageable) {
        List<CategoryDTO> categories = ((ICategoryFacade) facade).findByName(name, pageable);
        return ResponseEntity.ok(categories);
    }
}
