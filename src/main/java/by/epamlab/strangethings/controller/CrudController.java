package by.epamlab.strangethings.controller;

import by.epamlab.strangethings.dto.DTO;
import by.epamlab.strangethings.facade.Facade;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Provides methods for CRUD operations
 * @param <D> DTO type
 * @param <ID> ID field type
 */
public abstract class CrudController<D extends DTO<ID>, ID> {
    protected final Facade<D, ID> facade;

    protected CrudController(final Facade<D, ID> facade) {
        this.facade = facade;
    }

    /**
     * Returns entity DTO by specified id
     * @param id ID
     * @return entity with specified id
     */
    @GetMapping("/{id}")
    public ResponseEntity<D> get(@PathVariable final ID id) {
        D categories = facade.get(id);
        return ResponseEntity.ok(categories);
    }

    /**
     * Returns all present entities
     * @param page pageable
     * @return found entities DTO
     */
    @GetMapping("/all")
    public ResponseEntity<List<D>> getAll(final Pageable page) {
        List<D> entities = facade.getAll(page);
        return ResponseEntity.ok(entities);
    }

    /**
     * Adds entity according specified DTO
     * @param dto entity DTO
     * @return added entity DTO
     */
    @PostMapping("/add")
    public ResponseEntity<D> add(@RequestBody final D dto) {
        D added = facade.create(dto);
        return new ResponseEntity<>(added, HttpStatus.CREATED);
    }

    /**
     * Updates existing entity according specified DTO
     * @param dto entity DTO to update
     * @return updated entity DTO
     */
    @PostMapping("/update")
    public ResponseEntity<D> update(@RequestBody final D dto) {
        D updated = facade.update(dto);
        return ResponseEntity.ok(updated);
    }

    /**
     * Removes entity with specified id
     * @param id entity id
     */
    @PostMapping("/delete")
    public ResponseEntity<Void> delete(@RequestParam final ID id) {
        facade.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
