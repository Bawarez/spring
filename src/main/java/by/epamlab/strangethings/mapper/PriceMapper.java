package by.epamlab.strangethings.mapper;

import by.epamlab.strangethings.model.Price;
import by.epamlab.strangethings.dto.PriceDTO;
import org.springframework.stereotype.Service;

/**
 * Provides methods to map price model to DTO and vice versa
 */
@Service
public class PriceMapper implements EntityMapper<Price, PriceDTO, Long> {

    /**
     * Converts the specified price model to DTO
     * @param price price model to convert
     * @return price DTO
     */
    @Override
    public PriceDTO mapToDTO(final Price price) {
        PriceDTO priceDTO = new PriceDTO();
        priceDTO.setId(price.getId());
        priceDTO.setCurrency(price.getCurrency());
        priceDTO.setValue(price.getValue());

        return priceDTO;
    }

    /**
     * Converts the specified price DTO to model
     * @param dto price DTO to convert
     * @return price model
     */
    @Override
    public Price mapToModel(final PriceDTO dto) {
        return new Price(dto.getId(), dto.getCurrency(), dto.getValue());
    }
}
