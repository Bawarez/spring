package by.epamlab.strangethings.mapper;

import by.epamlab.strangethings.model.Category;
import by.epamlab.strangethings.repository.CategoryDAO;
import by.epamlab.strangethings.dto.CategoryDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Provides methods to map category model to DTO and vice versa
 */
@Service
public class CategoryMapper implements EntityMapper<Category, CategoryDTO, Long> {
    private final CategoryDAO categoryDAO;

    /**
     * Constructs a category mapper with the specified category DAO
     * @param categoryDAO category DAO
     */
    @Autowired
    public CategoryMapper(final CategoryDAO categoryDAO) {
        this.categoryDAO = categoryDAO;
    }

    /**
     * Converts the specified category model to DTO
     * @param category category model to convert
     * @return category DTO
     */
    @Override
    public CategoryDTO mapToDTO(final Category category) {
        Optional<Category> superCategory = Optional.ofNullable(category.getSuperCategory());
        Long superCategoryId = superCategory.orElse(new Category())
                .getId();

        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setId(category.getId());
        categoryDTO.setName(category.getName());
        categoryDTO.setSuperCategory(superCategoryId);

        return categoryDTO;
    }

    /**
     * Converts the specified category DTO to model
     * @param dto category DTO to convert
     * @return category model
     */
    @Override
    public Category mapToModel(final CategoryDTO dto) {
        Category superCategory = null;
        if (dto.getSuperCategory() != null) {
            superCategory = categoryDAO.findById(dto.getSuperCategory())
                .orElse(null);
        }
        return new Category(dto.getId(), dto.getName(), superCategory);
    }
}
