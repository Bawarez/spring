package by.epamlab.strangethings.mapper;

import by.epamlab.strangethings.model.Model;
import by.epamlab.strangethings.dto.DTO;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Provides methods to map model to DTO and vice versa
 * @param <M> model type
 * @param <D> DTO type
 * @param <ID> ID field type
 */
public interface EntityMapper<M extends Model, D extends DTO<ID>, ID> {
    /**
     * Converts the specified model to DTO
     * @param model model to convert
     * @return DTO
     */
    D mapToDTO(M model);

    /**
     * Converts the specified DTO to model
     * @param dto DTO to convert
     * @return model
     */
    M mapToModel(D dto);

    /**
     * Converts the specified list of models to a list of DTO
     * @param models models list
     * @return DTO list
     */
    default List<D> mapAllToDTO(Collection<M> models) {
        return models.stream()
                .map(this::mapToDTO)
                .collect(Collectors.toList());
    }
}
