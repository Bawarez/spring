package by.epamlab.strangethings.mapper;

import by.epamlab.strangethings.model.Category;
import by.epamlab.strangethings.model.Price;
import by.epamlab.strangethings.model.Product;
import by.epamlab.strangethings.dto.ProductDTO;
import by.epamlab.strangethings.repository.CategoryDAO;
import by.epamlab.strangethings.repository.PriceDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Provides methods to map product model to DTO and vice versa
 */
@Service
public class ProductMapper implements EntityMapper<Product, ProductDTO, Long> {
    private final PriceDAO priceDAO;
    private final CategoryDAO categoryDAO;

    /**
     * Constructs a product mapper with the specified price DAO and category DAO
     * @param priceDAO price DAO
     * @param categoryDAO category DAO
     */
    @Autowired
    public ProductMapper(final PriceDAO priceDAO, final CategoryDAO categoryDAO) {
        this.priceDAO = priceDAO;
        this.categoryDAO = categoryDAO;
    }

    /**
     * Converts the specified product model to DTO
     * @param product product model to convert
     * @return product DTO
     */
    @Override
    public ProductDTO mapToDTO(final Product product) {
        List<Long> categories = product.getCategories()
                .stream()
                .map(Category::getId)
                .collect(Collectors.toList());
        List<Long> prices = product.getPrices()
                .stream()
                .map(Price::getId)
                .collect(Collectors.toList());

        ProductDTO productDTO = new ProductDTO();
        productDTO.setId(product.getId());
        productDTO.setName(product.getName());
        productDTO.setPrices(prices);
        productDTO.setCategories(categories);

        return productDTO;
    }

    /**
     * Converts the specified product DTO to model
     * @param dto product DTO to convert
     * @return product model
     */
    @Override
    public Product mapToModel(final ProductDTO dto) {
        List<Price> prices = priceDAO.findAllById(dto.getPrices());
        List<Category> categories = categoryDAO.findAllById(dto.getCategories());
        return new Product(dto.getId(), dto.getName(), prices, categories);
    }
}
