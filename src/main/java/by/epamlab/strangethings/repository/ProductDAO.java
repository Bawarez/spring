package by.epamlab.strangethings.repository;

import by.epamlab.strangethings.model.Category;
import by.epamlab.strangethings.model.Price;
import by.epamlab.strangethings.model.Product;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Product repository
 */
@Repository
public interface ProductDAO extends ModelDAO<Product, Long> {

    /**
     * Retrieves products with the specified name from database
     * @param name name for search
     * @param pageable pageable
     * @return list of found products
     */
    List<Product> findAllByName(String name, Pageable pageable);

    /**
     * Retrieves products by the specified price id from database
     * @param price price id
     * @param pageable pageable
     * @return list of found products
     */
    List<Product> findAllByPrices(Price price, Pageable pageable);

    /**
     * Retrieves products by the specified category id from database
     * @param category category id
     * @param pageable pageable
     * @return list of found products
     */
    List<Product> findAllByCategories(Category category, Pageable pageable);
}
