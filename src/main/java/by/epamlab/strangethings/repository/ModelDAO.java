package by.epamlab.strangethings.repository;

import by.epamlab.strangethings.model.Model;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * Model repository
 * @param <M> model type
 * @param <ID> id field type
 */
@NoRepositoryBean
public interface ModelDAO<M extends Model, ID> extends JpaRepository<M, ID> {
}
