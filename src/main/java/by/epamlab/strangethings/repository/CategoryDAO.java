package by.epamlab.strangethings.repository;

import by.epamlab.strangethings.model.Category;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Category repository
 */
@Repository
public interface CategoryDAO extends ModelDAO<Category, Long> {
    /**
     * Retrieves all categories with the specified name from database
     * @param name category name
     * @param pageable pageable
     * @return list of retrieved categories
     */
    List<Category> findAllByName(String name, Pageable pageable);
}
