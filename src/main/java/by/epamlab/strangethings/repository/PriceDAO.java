package by.epamlab.strangethings.repository;

import by.epamlab.strangethings.model.Price;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Price repository
 */
@Repository
public interface PriceDAO extends ModelDAO<Price, Long> {
    /**
     * Retrieves prices by the specified currency from database
     * @param currency price currency
     * @param pageable pageable
     * @return list of found prices
     */
    List<Price> findAllByCurrency(String currency, Pageable pageable);

    /**
     * Retrieves prices with specified currency and value in range from min to max
     * @param currency currency for search
     * @param min minimal price value
     * @param max maximal price value
     * @param pageable pageable
     * @return list of found prices
     */
    List<Price> findAllByValueBetweenAndCurrency(int min, int max, String currency, Pageable pageable);
}
