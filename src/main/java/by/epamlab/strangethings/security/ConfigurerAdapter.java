package by.epamlab.strangethings.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.InMemoryUserDetailsManagerConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;

/**
 * Configures security
 */
@Configuration
public class ConfigurerAdapter extends WebSecurityConfigurerAdapter {
    private final AuthenticationEntryPoint authEntryPoint;

    /**
     * Constructs an instance with the specified authentication entry point
     * @param authEntryPoint authentication entry point
     */
    @Autowired
    public ConfigurerAdapter(final AuthenticationEntryPoint authEntryPoint) {
        this.authEntryPoint = authEntryPoint;
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.csrf().disable();

        http.authorizeRequests()
                .antMatchers("/*/add", "/*/update", "/*/delete").hasRole("ADMIN")
                .anyRequest().authenticated();

        http.httpBasic().authenticationEntryPoint(authEntryPoint);
    }

    /**
     * Returns a password encoder
     * @return password encoder
     */
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * configures application users details
     * @param auth authentication manager builder
     * @throws Exception if something goes wrong
     */
    @Autowired
    public void configureGlobal(final AuthenticationManagerBuilder auth) throws Exception {
        InMemoryUserDetailsManagerConfigurer<AuthenticationManagerBuilder>
                mngConfig = auth.inMemoryAuthentication();

        UserDetails u1 = User.withUsername("admin")
                .password(passwordEncoder().encode("admin"))
                .roles("ADMIN")
                .build();
        UserDetails u2 = User.withUsername("zakatnyj")
                .password(passwordEncoder().encode("123"))
                .roles("USER")
                .build();

        mngConfig.withUser(u1);
        mngConfig.withUser(u2);
    }

}
