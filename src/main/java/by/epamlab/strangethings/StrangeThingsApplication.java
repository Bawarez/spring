package by.epamlab.strangethings;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 * Application runner
 */
@SpringBootApplication
public class StrangeThingsApplication {

    /**
     * Programm entry point
     * @param args command line arguments
     */
    public static void main(final String[] args) {
        ApplicationContext ctx = SpringApplication.run(StrangeThingsApplication.class, args);
    }
}
